-- Срок: 2016-03-22 (100%), 2016-03-29 (50%)


module HW3
       ( foldr'
       , groupBy'
       , Either' (..)
       , either'
       , lefts'
       , rights'
       ) where
-- foldr' - правая свёртка
-- foldr' + b0 [an,...,a1,a0] =
--    = (an + ... + (a1 + (a0 + b0)))
foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' f b [] = b
foldr' f b as = foldr' f (f (last as) b) (init as)

-- Группировка
-- > groupBy' (==) [1,2,2,2,1,1,3]
-- [[1],[2,2,2],[1,1],[3]]
groupBy' :: (a -> a -> Bool) -> [a] -> [[a]]
groupBy' _ [] = [] 
groupBy' cmp (a:as) = (a:bs) : groupBy' cmp cs 
    where (bs,cs) = span (cmp a) as

-- Сумма типов a и b
data Either' a b = Left' a
                 | Right' b
                 deriving (Show)
-- Например, Either String b -
-- может быть результат типа b
-- или ошибка типа String

-- Из функций (a -> c) и (b -> c) получить функцию
-- над суммой a и b
-- Например,
-- > either' length id (Left' [1,2,3])
-- 3
-- > either' length id (Right' 4)
-- 4
-- > :t either' length id
-- Either [Int] Int -> Int
either' :: (a -> c) -> (b -> c) -> Either' a b -> c
either' f _ (Left' a) = f a
either' _ g (Right' b) = g b
-- Получение значений соответствующих слагаемых
lefts' :: [Either' a b] -> [a]
lefts' eabs = [eab | Left' eab <- eabs]

rights' :: [Either' a b] -> [b]
rights' eabs = [eab | Right' eab <- eabs]

